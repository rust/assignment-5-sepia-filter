#pragma once

#include <stdint.h>
#include <stdio.h>

#include "image.h"

struct __attribute__((packed)) bmp_header  {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum bmp_read_status {
    BMP_READ_OK = 0,
    BMP_READ_ERROR_IO,
    BMP_READ_ERROR_INVALID_SIGNATURE,
    BMP_READ_ERROR_UNSUPPORTED,
    BMP_READ_ERROR_UNEXPECTED_EOF,
};

char *bmp_read_status_message(enum bmp_read_status status);

enum bmp_read_status from_bmp(FILE *file, struct image *image);

enum bmp_read_status from_bmp_path(char *path, struct image *image);

enum bmp_write_status  {
    BMP_WRITE_OK = 0,
    BMP_WRITE_ERROR_IO,
};

char *bmp_write_status_message(enum bmp_write_status status);

enum bmp_write_status to_bmp(FILE *file, struct image const* image);

enum bmp_write_status to_bmp_path(char *path, struct image const* image);

