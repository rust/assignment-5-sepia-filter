#include <stdlib.h>
#include <math.h>

#include "sepia.h"

void filter_sepia(struct image *image) {
    struct pixel *p = image->data;;
    struct pixel *end = p + (image->width * image->height);
    while (p < end) {
        // convert 0-255 sRGB to 0-1 linear RGB
        float r = powf(p->r / 255.0, 2.2);
        float g = powf(p->g / 255.0, 2.2);
        float b = powf(p->b / 255.0, 2.2);

        // compute luminance
        float l = 0.2126 * r + 0.7152 * g + 0.0722 * b;

        // apply sepia color
        r = 0.97 * l;
        g = 0.69 * l;
        b = 0.28 * l;

        // convert 0-1 linear RGB to 0-255 sRGB
        p->r = powf(r, 1.0 / 2.2) * 255.0;
        p->g = powf(g, 1.0 / 2.2) * 255.0;
        p->b = powf(b, 1.0 / 2.2) * 255.0;

        p++;
    }
}
