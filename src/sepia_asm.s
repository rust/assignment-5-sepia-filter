section .text

global filter_sepia_asm
filter_sepia_asm:
	mov rax, [rdi+16]
	; rax - current pixel

	mov rcx, [rdi]		; width
	imul rcx, [rdi+8]	; height
	lea rcx, [rcx*2+rcx]
	add rcx, rax
	; rcx - max address

.loop:
	; load 4 bytes zero-extending to 4 32-bit integers
	pmovzxbd xmm0, [rax]
	; convert to floats
	cvtdq2ps xmm0, xmm0

	; map to 0-1 range
	divps xmm0, [.c4]

	; approximation for x^2.2
	sqrtps xmm1, xmm0
	addps xmm1, [.c0]
	mulps xmm0, xmm0
	mulps xmm0, [.c1]
	mulps xmm0, xmm1

	; compute luma
	mulps xmm0, [.c5]

	; horizontal sum
	movshdup xmm1, xmm0
    addps xmm0, xmm1
    movhlps xmm1, xmm0
    addss xmm0, xmm1

	; apply sepia color
	shufps xmm0, xmm0, 0
	mulps xmm0, [.c6]

	; approximation for x^(1/2.2)
	sqrtps xmm1, xmm0
	mulps xmm1, [.c2]
	mulps xmm0, [.c3]
	subps xmm1, xmm0

	; map to 0-255 range
	mulps xmm1, [.c4]

	; convert 4 floats to 4 bytes
	cvtps2dq xmm1, xmm1
	packusdw xmm1, xmm1
	packuswb xmm1, xmm1

	; store 3 bytes
	mov sil, [rax+3]
	movd [rax], xmm1
	mov [rax+3], sil

	add rax, 3
	cmp rax, rcx
	jl .loop

	ret

	align 16
.c0:
	times 4 dd 1.25061
.c1:
	times 4 dd 0.445214
.c2:
	times 4 dd 1.12661
.c3:
	times 4 dd 0.126615
.c4:
	times 4 dd 255.0
.c5:
	dd 0.0722
	dd 0.7152
	dd 0.2126
	dd 0.0
.c6:
	dd 0.28
	dd 0.69
	dd 0.97
	dd 0.0
