#pragma once

#include "image.h"

void filter_sepia(struct image *image);

void filter_sepia_asm(struct image *image);
