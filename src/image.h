#pragma once

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void image_init(struct image *image, uint64_t width, uint64_t height);

void image_free(struct image *image);

struct pixel image_get(struct image const* image, uint64_t x, uint64_t y);

void image_set(struct image *image, uint64_t x, uint64_t y, struct pixel pixel);
