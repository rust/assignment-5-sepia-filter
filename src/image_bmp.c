#include <errno.h>
#include <string.h>

#include "image_bmp.h"

#define BMP_MAGIC 0x4d42

static enum bmp_read_status validate_header(struct bmp_header *header) {
    if (header->bfType != BMP_MAGIC)
        return BMP_READ_ERROR_INVALID_SIGNATURE;
    
    if (header->biPlanes != 1)
        return BMP_READ_ERROR_UNSUPPORTED;

    if (header->biBitCount != 24)
        return BMP_READ_ERROR_UNSUPPORTED;

    if (header->biCompression != 0)
        return BMP_READ_ERROR_UNSUPPORTED;

    return BMP_READ_OK;
}

static size_t compute_padding(size_t width) {
    return width % 4 == 0 ? 0 : 4 - (width * 3) % 4;
}

const char* BMP_READ_STATUS_i[] = {
    "BMP_READ_OK",
    "BMP_READ_ERROR_IO",
    "BMP_READ_ERROR_INVALID_SIGNATURE",
    "BMP_READ_ERROR_UNSUPPORTED",
    "BMP_READ_ERROR_UNEXPECTED_EOF",
};

char *bmp_read_status_message(enum bmp_read_status status) {
    switch (status) {
    case BMP_READ_OK:
        return "OK";
    case BMP_READ_ERROR_IO:
        return strerror(errno);
    case BMP_READ_ERROR_INVALID_SIGNATURE:
        return "Invalid BMP signature";
    case BMP_READ_ERROR_UNSUPPORTED:
        return "Unsupported BMP";
    case BMP_READ_ERROR_UNEXPECTED_EOF:
        return "Unexpected EOF";
    default:
        return "Unknown error";
    }
}

enum bmp_read_status from_bmp(FILE *file, struct image *image) {
    struct bmp_header header;
    enum bmp_read_status status;
    
    if (fread(&header, sizeof(struct bmp_header), 1, file) != 1)
        return BMP_READ_ERROR_IO;

    if ((status = validate_header(&header)) != BMP_READ_OK)
        return status;

    if (fseek(file, header.bOffBits, SEEK_SET) != 0)
        return BMP_READ_ERROR_IO;

    image_init(image, header.biWidth, header.biHeight);

    size_t padding = compute_padding(header.biWidth);
    struct pixel *data = image->data;
    
    for (uint32_t y = 0; y < header.biHeight; y++) {
        if (y == header.biHeight - 1) padding = 0;

        size_t count = header.biWidth * 3 + padding;
        if (fread(data, 1, count, file) != count)
            return BMP_READ_ERROR_UNEXPECTED_EOF;

        data += header.biWidth;
    }

    return BMP_READ_OK;
}

enum bmp_read_status from_bmp_path(char *path, struct image *image) {
    FILE *file = fopen(path, "rb");
    if (file == NULL)
        return BMP_READ_ERROR_IO;
    enum bmp_read_status status = from_bmp(file, image);
    fclose(file);
    return status;
}

char *bmp_write_status_message(enum bmp_write_status status) {
    switch (status) {
    case BMP_WRITE_OK:
        return "OK";
    case BMP_WRITE_ERROR_IO:
        return strerror(errno);
    default:
        return "Unknown error";
    }
}

enum bmp_write_status to_bmp(FILE *file, struct image const* image) {
    size_t padding = compute_padding(image->width);
    struct pixel *data = image->data;

    struct bmp_header header = {
        .bfType = BMP_MAGIC,
        .bfileSize = sizeof(struct bmp_header)
            + (image->width * 3 + padding) * image->height,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = 0,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };
    
    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1)
        return BMP_WRITE_ERROR_IO;

    for (uint32_t y = 0; y < header.biHeight; y++) {
        if (y == header.biHeight - 1) padding = 0;

        size_t count = header.biWidth * 3 + padding;
        if (fwrite(data, 1, count, file) != count)
            return BMP_WRITE_ERROR_IO;


        data += header.biWidth;
    }

    return BMP_WRITE_OK;
}

enum bmp_write_status to_bmp_path(char *path, struct image const* image) {
    FILE *file = fopen(path, "wb");
    if (file == NULL)
        return BMP_WRITE_ERROR_IO;
    enum bmp_write_status status = to_bmp(file, image);
    fclose(file);
    return status;
}
