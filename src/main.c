#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

#include "image_bmp.h"
#include "sepia.h"

int main(int argc, char **argv) {
    int option;
    bool is_asm_impl = false;

    while((option = getopt(argc, argv, "a")) != -1) {
        switch (option) {
            case 'a':
                is_asm_impl = true;
                break;
            default:
                break;
        }
    }

    if (optind + 1 >= argc) {
        fprintf(stderr, "Usage: %s [-a] <in> <out>\n", argv[0]);
        return EXIT_FAILURE;
    }

    char *input_path = argv[optind];
    char *output_path = argv[optind + 1];
    
    enum bmp_read_status read_status;
    enum bmp_write_status write_status;

    struct image image = {0};
    if ((read_status = from_bmp_path(input_path, &image)) != BMP_READ_OK) {
        fprintf(stderr, "Read error: %s.\n", bmp_read_status_message(read_status));
        image_free(&image);
        return EXIT_FAILURE;
    }

    clock_t start, end;

    start = clock();

    if (is_asm_impl) {
        filter_sepia_asm(&image);
    } else {
        filter_sepia(&image);
    }

    end = clock();

    float cpu_time_used = ((float) (end - start)) / CLOCKS_PER_SEC;
    printf("took %fms\n", cpu_time_used * 1000.0);

    if ((write_status = to_bmp_path(output_path, &image)) != BMP_WRITE_OK) {
        fprintf(stderr, "Write error: %s.\n", bmp_write_status_message(write_status));
        image_free(&image);
        return EXIT_FAILURE;
    }
    
    image_free(&image);
    return EXIT_SUCCESS;
}
